The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?



```scala
def primes: LazyList[Int] = {
    def loop(curr: Int, past: collection.immutable.Queue[Int]): LazyList[Int] = {
        val max = math.sqrt(curr.toDouble)
        past.takeWhile(_ <= max)
            .find(p => curr % p == 0) match {
                case Some(_) => loop(curr + 2, past)
                case None => curr #:: loop(curr + 2, past.enqueue(curr))
            }
    }
                  
    2 +: loop(3, collection.immutable.Queue())
} 
```




    defined [32mfunction[39m [36mprimes[39m




```scala
def factors(n: Long): Seq[Int] = { 
    val prms = primes 
    def loop(rem: Long, divs: collection.immutable.Queue[Int], ps: LazyList[Int]): LazyList[Int] = { 
        if (rem == 1) LazyList.empty 
        else if (ps.isEmpty) loop(rem, divs, prms) 
        else if (rem % ps.head == 0) ps.head #:: loop(rem / ps.head, divs.enqueue(ps.head), ps) 
        else loop(rem, divs, ps.tail) 
    } 
    loop(n, collection.immutable.Queue(), prms).sortWith(_ > _).toList 
} 

```




    defined [32mfunction[39m [36mfactors[39m




```scala
// test
factors(13195)
```




    [36mres11[39m: [32mSeq[39m[[32mInt[39m] = [33mList[39m([32m29[39m, [32m13[39m, [32m7[39m, [32m5[39m)




```scala
factors(600851475143L)
```




    [36mres12[39m: [32mSeq[39m[[32mInt[39m] = [33mList[39m([32m6857[39m, [32m1471[39m, [32m839[39m, [32m71[39m)


